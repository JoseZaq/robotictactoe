package sockets.conexion

import java.io.DataOutputStream
import java.net.ServerSocket
import java.net.Socket

open class Conexion(tipo: String) {
    private val PUERTO = 5000 //Puerto para la conexión
    private val HOST = "192.168.0.18" //Host para la conexión
    protected var mensajeServidor //Mensajes entrantes (recibidos) en el servidor
            : String? = null
    protected var ss //Socket del servidor
            : ServerSocket? = null
    protected var cs //Socket del cliente
            : Socket? = null
    protected var salidaServidor: DataOutputStream? = null
    protected var salidaCliente //Flujo de datos de salida
            : DataOutputStream? = null

    init  //Constructor
    {
        if (tipo.equals("servidor", ignoreCase = true)) {
            ss = ServerSocket(PUERTO) //Se crea el socket para el servidor en puerto 1234
            cs = Socket() //Socket para el cliente
        } else {
            cs = Socket(HOST, PUERTO) //Socket para el cliente en localhost en puerto 1234
        }
    }
}