package sockets.servidor

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import sockets.conexion.Conexion
import java.io.BufferedReader
import java.io.DataOutputStream
import java.io.InputStreamReader
import java.io.PrintWriter
import java.lang.StringBuilder

class Servidor(val channel: Channel<MutableList<Float>>) //Se usa el constructor para servidor de Conexion
    : Conexion("servidor") //Se hereda de conexión para hacer uso de los sockets y demás
{
    fun startServer() //Método para iniciar el servidor
    {
        try {
            println("Esperando...") //Esperando conexión
            cs = ss?.accept() //Accept comienza el socket y espera una conexión desde un cliente
            println("Cliente en línea")

            //Se obtiene el flujo de salida del cliente para enviarle mensajes
            salidaCliente = DataOutputStream(cs?.getOutputStream())
            //Se le envía un mensaje al cliente usando su flujo de salida
            salidaCliente!!.writeUTF("Petición recibida y aceptada")

            //Se obtiene el flujo entrante desde el cliente
            val entrada = BufferedReader(InputStreamReader(cs?.getInputStream()))
            while (entrada.readLine().also { mensajeServidor = it } != null) //Mientras haya mensajes desde el cliente
            {

                // enviar la posicion (x,y, player)  del punto donde hay que colocar la siguiente pieza
                sendMovementOnChange()
                //
                // Clave secreta
                println(mensajeServidor)
                if (mensajeServidor.equals("JORDI"))
                    salidaCliente!!.writeUTF("JORDIES AROUND THE WORLD")
            }
            println("Fin de la conexión")
            ss?.close() //Se finaliza la conexión con el cliente
        } catch (e: Exception) {
            println(e.message)
        }
    }

    fun sendMovementOnChange() = runBlocking {
        withContext(Dispatchers.IO) {
            val position = channel.receive()
            val builder = StringBuilder()
            // organize elements
            position.forEachIndexed { i, element ->
                if(i == 0)
                    builder.append(element)
                else
                    builder.append(",").append(element)
            }
            // send elements
            println("la posicion enviada: "+ builder.toString())
            salidaCliente?.writeUTF("("+builder.toString()+")")
        }
    }
}