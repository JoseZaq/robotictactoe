package sockets.main

import kotlinx.coroutines.channels.Channel
import sockets.servidor.Servidor
import java.io.IOException
import kotlin.properties.Delegates

//Clase principal que hará uso del servidor
class MainSocketService(val channel: Channel<MutableList<Float>>) {

    private var _tile = -1
    var tile: Int by Delegates.observable(_tile) { prop, old, new ->
        print("JORDI")
        _tile = new
    }

    fun startServer() {
        thread(start = true) {
            println("${Thread.currentThread()} has run.") //delete when uncessasry
            val serv = Servidor(channel) //Se crea el servidor
            println("Iniciando servidor\n")
            serv.startServer() //Se inicia el servidor
        }
    }

    private fun thread(
        start: Boolean = true,
        isDaemon: Boolean = false,
        contextClassLoader: ClassLoader? = null,
        name: String? = null,
        priority: Int = -1,
        block: () -> Unit
    ): Thread {
        val thread = object : Thread() {
            public override fun run() {
                block()
            }
        }
        if (isDaemon)
            thread.isDaemon = true
        if (priority > 0)
            thread.priority = priority
        if (name != null)
            thread.name = name
        if (contextClassLoader != null)
            thread.contextClassLoader = contextClassLoader
        if (start)
            thread.start()
        return thread
    }
}