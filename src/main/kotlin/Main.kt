import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import kotlinx.coroutines.channels.Channel
import sockets.main.MainSocketService
import sockets.servidor.Servidor
import java.io.IOException

object Main {
    @Throws(IOException::class)
    @JvmStatic
    fun main(args: Array<String>) {
        // vars
        val channel = Channel<MutableList<Float>>()
        // inizialize server
        MainSocketService(channel).startServer()
        // inizialize game
        MainGame(channel).start()
    }
}