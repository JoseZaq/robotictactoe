data class Board(val board : List<MutableList<Boolean?>> = List(3){MutableList(3){null}}, var isCirclePlayer : Boolean = true) {

    var circleList = mutableListOf(0);
    var crossList = mutableListOf(0);
//    var map: HashMap<String, Int>
    var winnerCombos: List<List<Int>> = listOf(
        listOf(1,4,7),
        listOf(2,5,8),
        listOf(3,6,9),
        listOf(1,2,3),
        listOf(4,5,6),
        listOf(7,8,9),
        listOf(1,5,9),
        listOf(3,5,7),
    )
    var robotNextMove: MutableList<Int> = mutableListOf(0, 0)
    var gameFinished = false

    fun playAt(x: Int, y: Int){
        if(board[y][x]==null) {
            board[y][x] = isCirclePlayer
            isCirclePlayer = !isCirclePlayer
        }
    }

    fun checkTiles() : Boolean { // CHECK IF A MOVEMENT IS POSSIBLE
        board.forEachIndexed { i, row ->
            row.forEachIndexed {j, tile ->
                if (tile == null) {
                    robotNextMove[0] = j
                    robotNextMove[1] = i
                    return true
                }
            }
        }
        return false
    }

    fun check3TilesInARow(): Boolean {
        winnerCombos.forEach { combo ->
            var isJordi = circleList.containsAll(combo)
            var isSquareJordi = crossList.containsAll(combo)
            if (isJordi || isSquareJordi) { // Warning (Jordi)
                gameFinished = true
                return true
            }
        }
        return false
    }

    /**
     * add position in x or o list
     */
    fun saveMove(x: Int, y: Int) {
        if(isCirclePlayer) circleList.add((x * 3 )+y + 1)
        else crossList.add((x * 3 )+y + 1)
    }

    fun finish() {
        println("END GAME")
        gameFinished = true
    }
}