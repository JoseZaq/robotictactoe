// Copyright 2000-2021 JetBrains s.r.o. and contributors. Use of this source code is governed by the Apache 2.0 license that can be found in the LICENSE file.
import androidx.compose.material.MaterialTheme
import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.selection.selectable
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.awt.ComposeWindow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.FrameWindowScope
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlin.system.exitProcess


public class MainGame(val channel: Channel<MutableList<Float>>) {
    var isRestartable = false

    fun start() = application {
        Window(onCloseRequest = ::exitApplication) {
            App()
        }
    }

    fun moveTo(x: Int, y: Int, isCirclePlayer: Boolean, board: Board){
        // MOVE ROBOTO
        println("$x $y $isCirclePlayer")
        // check winner
        if (board.check3TilesInARow()) {
            println("WINNER " + if(board.isCirclePlayer) "Circle" else "Cross")
            sendFinishMessageToRobot(board.isCirclePlayer)
            board.finish()
        } else if(board.checkTiles()) {
            println("MOVIMIENTO POSIBLE\nx: ${board.robotNextMove[0]}, y: ${board.robotNextMove[1]}")
            sendMovementToRobot(x.toFloat(), y.toFloat(),if(isCirclePlayer) 0f else 1f)
        }
        else {
            board.finish()
            sendFinishMessageToRobot()
        }
    }

    private fun sendRestartMessageToRobot() = runBlocking {
        // send restart message
        launch { channel.send(mutableListOf(10f,10f,10f))}
        // todo:restart window game

    }

    private fun sendFinishMessageToRobot(isCirclePlayer: Boolean? = null) = runBlocking {
        launch {
            if(isCirclePlayer == null)
            channel.send(listOf(-1f,-1f, -1f) as MutableList<Float>)
            else
                channel.send(listOf(-1f,-1f, if(isCirclePlayer) 0f else 1f) as MutableList<Float>)
        }
        isRestartable = true
    }

    private fun sendMovementToRobot(x:Float, y: Float, player: Float) = runBlocking {
        launch { channel.send(listOf(x,y,player) as MutableList<Float>) }
    }

    @Composable
    @Preview
    fun App() {
        var board by remember { mutableStateOf(Board()) }
        MaterialTheme {
            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                board.board.forEachIndexed { y, row ->
                    Row() {
                        row.forEachIndexed { x, cell ->
                            val text = when(cell){
                                true -> "X"
                                false -> "0"
                                else -> " "
                            }
                            Box(modifier = Modifier.width(80.dp).height(80.dp).border(1.dp, Color.Black).clickable {
                                board.playAt(x, y)
                                board.saveMove(x,y)
                                val aux = board
                                board = Board()
                                board = aux
                                moveTo(x,y,board.isCirclePlayer, board)
                            }) {Text(
                                text,
                                modifier = Modifier.align(Alignment.Center),
                                textAlign = TextAlign.Center,
                                fontSize = 25.sp,
                            )
                            }
                        }
                    }
                }
                Box(modifier = Modifier.width(240.dp).height(40.dp).border(2.dp, Color.Red).selectable(true,isRestartable, onClick = {
                    board = Board()
                    isRestartable = false
                    sendRestartMessageToRobot()
                } )) {Text(
                    "Reiniciar",
                    modifier = Modifier.align(Alignment.Center),
                    textAlign = TextAlign.Center,
                    fontSize = 25.sp,
                )
                }

            }
        }
    }
}

